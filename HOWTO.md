# Processo Seletivo Hepta - Gabriel

1) Realizado as implementa��es Rest (Classes Java);

2) Criei um m�todo para busca dos fabricantes, para popular a combo fabricante (cadastro/edi��o). A ideia tamb�m era permitir o cadastro de um novo fabricante diretamente da tela de cadastro do item, mas n�o deu tempo fazer a implementa��o;

3) Infelizmente, n�o consegui concluir a parte da edi��o e testes;

4) Cadastro, Listagem e Exclus�o OK;

5) Apesar de n�o ser necess�rio (segundo a documenta��o do VueJS), adicionei o jQuery. Como gosto muito de trabalhar com o console do Chrome e, estava gerando um erro, por isso o adicionei;

6) No cadastro, est� validando todos os campos como obrigat�rio;

7) No persistence.xml alterei o usuario e alguns detalhes da conex�o, pois ao instalar o MySQL 8 e tentar fazer a conex�o, gerou erros (query_cache_size e caching_sha2_password); Fiz algumas altera��es no MySQL e ficou OK;

8) Dei uma alterada no visual, para deixar mais bonito. ;-)

# Ambiente utilizado
Eclipse 2019-06;
Maven 3.6.1;
Java 1.8; Tomcat 8.5.
