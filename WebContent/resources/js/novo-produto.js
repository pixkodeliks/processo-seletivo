new Vue({
	el: "#cadastroApp",

	  data:{
		  listaFabricantes: [],
		  errors: [],
		  item: "",
		  volume: "",
		  unidade: "",
		  estoque: "",
	      log: "",
	      fabricante: ""
	  },
	  
	  created: function(){
	      let vm =  this;
	      vm.buscarFabricantes();
	    
	  },
	  
	  methods: {
	      
		  buscarFabricantes: function(){
				const vm = this;
				axios.get("/mercado/rs/fabricante")
				.then(response => {
					vm.listaFabricantes = response.data;
				}).catch(function (error) {
					vm.mostraAlertaErro("Erro interno", "Não foi listar natureza de serviços");
				}).finally(function() {
				});
			},
			
			
		  sub: function(event){
	          
			  this.errors = [];

		      if (!this.item) {
		          this.errors.push('O Nome do Item é obrigatório.');
		        }
		      if (!this.volume) {
		          this.errors.push('O Volume é obrigatório.');
		        }
		      if (!this.estoque) {
		          this.errors.push('A Qtd. em Estoque é obrigatório.');
		        }		      
		      if (!this.unidade) {
		        this.errors.push('A Unidade é obrigatória.');
		      }			  
			  
		    			
		      if (this.errors.length > 0) {
				  event.preventDefault(); 
			  
	          }else{
	         
	            axios.post('/mercado/rs/produtos', {estoque: this.estoque,  fabricante:{"id": this.fabricante}, nome: this.item, unidade: this.unidade, volume: this.volume})
	            .then(response => {
	                	                
	                console.log(response.status);
	                alert('Produto inserido com sucesso.');
	            })
	            .catch(error => {
	                console.log(error)
	            })           
	            
	            
	          }   
	      }
	  }
	  
	});