var inicio = new Vue({
	el:"#inicio",
    data: {
        listaProdutos: [],
        idProduto: '',
        listaProdutosHeader: [
			{sortable: false, key: "nome", label:"Nome"},
			{sortable: false, key: "fabricante.nome", label:"Fabricante"},
			{sortable: false, key: "volume", label:"Volume"},
			{sortable: false, key: "unidade", label:"Unidade"},
			{sortable: false, key: "estoque", label:"Estoque"}
		]
    },
    created: function(){
        let vm =  this;
        vm.buscaProdutos();
    },
    methods:{
        buscaProdutos: function(){
			const vm = this;
			axios.get("/mercado/rs/produtos")
			.then(response => {
				vm.listaProdutos = response.data;
			}).catch(function (error) {
				vm.mostraAlertaErro("Erro interno", "Não foi listar natureza de serviços");
			}).finally(function() {
			});
		},
		
		deleteProduto(id){
						
			const vm = this;
			
			const url = '/mercado/rs/produtos/'+id;		   
			axios.delete(url).then(function (response){
				vm.buscaProdutos();
				$('#modalExclusao').modal('hide');
				
			}).catch(function (error) {
			    // handle error
			    console.log(error);
			  })
		}
    }
});